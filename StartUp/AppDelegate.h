//
//  AppDelegate.h
//  StartUp
//
//  Created by Roberto Frontado on 8/6/14.
//  Copyright (c) 2014 frontado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
