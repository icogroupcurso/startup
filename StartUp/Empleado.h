//
//  Empleado.h
//  StartUp
//
//  Created by Roberto Frontado on 8/6/14.
//  Copyright (c) 2014 frontado. All rights reserved.
//

#import "Persona.h"
      //Despues de ":" Se coloca la clase de la que se hereda, en este caso Persona.
@interface Empleado : Persona

@property (nonatomic) NSString* tipo;
@property (nonatomic) NSInteger salario;

@end
