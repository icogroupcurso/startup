//
//  ViewController.m
//  StartUp
//
//  Created by Roberto Frontado on 8/6/14.
//  Copyright (c) 2014 frontado. All rights reserved.
//

#import "ViewController.h"
#import "StartUp.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark - Ciclo de vida de la vista
//En orden de ejecucion

-(void)viewWillAppear:(BOOL)animated{
    //Se ejecuta antes de que la vista se muestre
}

- (void)viewDidLoad
{
    
    StartUp* icoGroup; // Puedes inicializar al instanciar la variable
    // StartUp* startUp = [[StartUp alloc] init];
    
    StartUp* otroStartUp;
    
    StartUp* aunOtroStartUpMas;
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    icoGroup = [[StartUp alloc] init]; // ó su equivalente - startup = [StartUp new]
    [icoGroup setName:@"UnNombre"]; // ó su equivalente - icoGroup.name = @"UnNombre";
    [icoGroup setFounded:2008]; // ó su equivalente - icoGroup.founded = 2008;
    
    NSLog(@"NSLog de ViewController:");
    NSLog(@"Nombre de la empresa: %@ Año de fundacion: %i", [icoGroup name], [icoGroup founded]);
    // o tambien
    //NSlog(@"Nombre de la empresa: %@ Año de fundacion: %i", icoGroup.name, icoGroup.founded);
    //Para poder usar objetos dentro de un string se coloca con %@, en este caso un NSString es un objeto
    
    NSLog(@"\n");//Para salto de linea en la consola;
    
    NSLog(@"showInfo de icoGroup - Metodo de la instancia de StartUp");
    [icoGroup showInfo];
    NSLog(@"\n");//Para salto de linea en la consola;
    
    //Las objetos pueden re-inicializarse
    icoGroup = [[StartUp alloc] initStartUpWithName:@"OtroNombre" andFoundedYear:2099];
    NSLog(@"Objeto despues de initStartUpWithName:andFoundedYear:");
    [icoGroup showInfo];
    NSLog(@"\n");//Para salto de linea en la consola;
    
    //-------------------------------------------------------------------------
    
    otroStartUp = [[StartUp alloc] initStartUpWithName:@"OtroStartUpMas(OSUM)" andFoundedYear:1980];
    NSLog(@"ShowInfo de OSUM");
    [otroStartUp showInfo];   
    NSLog(@"\n");//Para salto de linea en la consola;
    //-------------------------------------------------------------------------

    aunOtroStartUpMas = [StartUp startUpWithName:@"AunOtroStartUpMas(AOSUM)" withFoundedYear:1556];
    NSLog(@"ShowInfo de AOSUM");
    [aunOtroStartUpMas showInfo];
    NSLog(@"\n");//Para salto de linea en la consola;
    
}

-(void)viewDidAppear:(BOOL)animated{
    //Se ejecuta despues de que la vista se mostro
}

-(void)viewWillDisappear:(BOOL)animated{
    //Se ejecuta antes de que la vista desaparezca
}

-(void)viewDidDisappear:(BOOL)animated{
    //Se ejecuta despues de que la vista desaparecio
}



#pragma mark Fin Ciclo de vida de la vista -






- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
