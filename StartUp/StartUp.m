//
//  StartUp.m
//  StartUp
//
//  Created by Roberto Frontado on 8/6/14.
//  Copyright (c) 2014 frontado. All rights reserved.
//

#import "StartUp.h"

@implementation StartUp

//Metodo de la clase empiezan con "+"
/*
 * @description inicializa una instancia de StartUp con name y founded
 * @param name : NSString - nombre del StartUp
 * @param founded: NSInteger - fecha de fundacion del StartUp
 * @return Instancia inicializada de StartUp con name y founded
 */
+(instancetype)startUpWithName:(NSString*)name withFoundedYear:(NSInteger)founded{
    return [[StartUp alloc] initStartUpWithName:name andFoundedYear:founded];
}

-(instancetype)init{
    self = [super init];
    
    //Este if valida si existe el objeto, es decir - if self != nil
    if (self) {
        
    }
    
    return  self;
}

//Metodo constructor personalizado
/*
 * @description inicializa una instancia de StartUp con name y founded
 * @param name : NSString - nombre del StartUp
 * @param founded: NSInteger - fecha de fundacion del StartUp
 * @return Instancia inicializada de StartUp con name y founded
 */
-(instancetype)initStartUpWithName:(NSString*)name andFoundedYear:(NSInteger)founded{
    
    self = [self init];
    
    _name = name;
    /* Formas alternativas:
     self.name = name;
     [self setName:name];
     */
    
    _founded = founded;
    /* Formas alternativas:
     self.founded = founded;
     [self setFounded:founded];
     */
    
    return self;
}


-(void)showInfo{
    
    NSLog(@"Nombre de la empresa: %@ Año de fundacion: %i", _name, _founded);
    // o tambien
    //NSLog(@"Nombre de la empresa: %@ Año de fundacion: %i", self.name, self.founded);
    //NSLog(@"Nombre de la empresa: %@ Año de fundacion: %i", [self name], [self founded]);
}


@end
