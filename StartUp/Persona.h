//
//  Persona.h
//  StartUp
//
//  Created by Roberto Frontado on 8/6/14.
//  Copyright (c) 2014 frontado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Persona : NSObject

@property (nonatomic) NSString* nombre,
                              * apellido;
@property (nonatomic) NSInteger edad;

@end
