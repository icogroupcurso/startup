//
//  StartUp.h
//  StartUp
//
//  Created by Roberto Frontado on 8/6/14.
//  Copyright (c) 2014 frontado. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface StartUp : NSObject


// Propiedad = atributo de tipo publico
@property (nonatomic, strong) NSString* name;
@property (nonatomic, assign) NSInteger founded;

//Metodos publicos de la clase empiezan con "+"
+(instancetype)startUpWithName:(NSString*)name withFoundedYear:(NSInteger)founded;

//Metodos de inicializacion de la clase deben ser publicos
-(instancetype)initStartUpWithName:(NSString*)name andFoundedYear:(NSInteger)founded;

//Metodos publicos de instancias de la clase empiezan con "-"
-(void)showInfo; //Declaracion publica del metodo showInfo

@end
